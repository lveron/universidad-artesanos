
window.onload = mostrar();

function mostrar() {
    var filtronombre=$("#filtronombre").val(); 
    var filtrocategoria=$("#filtrocategoria").val(); 
    var cuadro = "<div class='row'>";
    var contador= 0;   
    document.getElementById('mapid').innerHTML = "<div id='map'></div>";
    var location = [ -34.521166, -58.700661];
    var map = L.map('map').setView(location, 15);
  
    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors '
    }).addTo(map);

    //filtrado de búsqueda
    for (i = 0; i < talleres.length; i++) { 
        if ( talleres[i].nombre.toLowerCase().includes(filtronombre.toLowerCase())){
            if ( talleres[i].categoria.includes(filtrocategoria)){ 
                var y =  talleres[i].coodenada_x ;
                var x =  talleres[i].coodenada_y ;
                var nombre =  talleres[i].nombre ;
                var id =  talleres[i].id ;
                var location = [y , x];
                map.setView(location, 15);
                var Marker=L.marker(location).bindPopup(talleres[i].nombre);
                Marker.addTo(map);

                var cuadro = cuadro + "<div class='col-lg-12 mb-3'> <div class='card w-100' id="+id+" onclick=centrarmapa("+y+","+x+","+id+")>";       
                var cuadro = cuadro + "";
                var cuadro = cuadro + "<div class='card-body'>";
                var cuadro = cuadro + "<h4 class='card-title'>";
                var cuadro = cuadro + "<a  >"+nombre+"</a> </h4>";
                var cuadro = cuadro + "<p>"+talleres[i].direccion+"</p>";
                var cuadro = cuadro + "<div  id='info"+id+"' style='display:none;'><a><img class='card-img-top imagen' src="+talleres[i].foto+"></a> <p> <b>Telefono:</b> "+talleres[i].telefono+"</p> <p> <b>Horarios:</b> "+talleres[i].horario+"</p> <p> <b>Categorías:</b> "+talleres[i].categoria+"</p> <p> <b>Actividades ofrecidas:</b> "+talleres[i].actividades+"</p> <p> "+talleres[i].descripcion+"</p> </div>	";
                var cuadro = cuadro + "</div>";
                var cuadro = cuadro + "</div>";
                var cuadro = cuadro + "</div>";
                contador = contador + 1;
            }
        }
    }
    if (contador == 0){
        cuadro = "<h5>Sin Resultados</h5> ";
    }
    var cuadro = cuadro + "</div>";
    document.getElementById("vertalleres").innerHTML = cuadro;
}
//centrado de mapa con las coordenadas y el id.
function centrarmapa(var1, var2, var3)  {
    var ubicacion = [var1 , var2];
    mostrar();      //restaurar 
    document.getElementById('mapid').innerHTML = "<div id='map'></div>";
    var map = L.map('map').setView(ubicacion, 15);
    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);

    var Marker=L.marker(ubicacion).bindPopup('Ubicación del <b>Taller</b> seleccionado'); 
    Marker.addTo(map);
   
    var intro = document.getElementById(var3);
    var aux = "#info"+var3;
    intro.style.backgroundColor = "rgba(117, 131, 190, 0.623)"; 

    $(aux).toggle("slow");
    
}




