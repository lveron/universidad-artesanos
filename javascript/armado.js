
$(document).ready(function() {
    armarBarraNavegacion();
    armarFooter();
});

function armarBarraNavegacion() {
    $('#barraNavegacion').html("    <nav class='navbar navbar-expand-lg navbar-blue bg-blue fixed-top'>     <div class='container'>      <a class='navbar-brand'>Artesanos</a>      <button class='navbar-toggler' type='button' data-toggle='collapse' data-target='#navbarResponsive' aria-controls='navbarResponsive' aria-expanded='false' aria-label='Toggle navigation'>        <span class='navbar-toggler-icon'></span>      </button>      <div class='collapse navbar-collapse' id='navbarResponsive'>        <ul class='navbar-nav ml-auto'>          <li class='nav-item active'>            <a class='nav-link' href='formulario.html'>Incio              <span class='sr-only'></span>            </a>          </li>          <li class='nav-item'>            <a class='nav-link' href='#'>Mi Cuenta</a>          </li>          <li class='nav-item'>            <a class='nav-link' href='#'>Buscar Artesanía</a>          </li>          <li class='nav-item'>            <a class='nav-link' href='#' >Salir</a>          </li>        </ul>      </div>    </div>  </nav>");
}

function armarFooter() {
    $('#footer').html("   <footer class='py-5 bg-blue'>    <div class='container'>      <p class='m-0 text-center text-white'>Copyright &copy; Tp Grupo 6</p>    </div>  </footer>");
}


